def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float :
    """
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    return 0


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
    return {}


class Lasagna:
    def bake_time_remaining(self, time_elapsed: int) -> int:
        """
        Function that takes the actual minutes the lasagna has been in the oven as an argument
        and returns how many minutes the lasagna still needs to bake based on the EXPECTED_BAKE_TIME
        """
        return 0

    def preparation_time_in_minutes(self, nb_layers: int) -> int:
        """
         Takes the number of layers you want to add to the lasagna as an argument
         and returns how many minutes you would spend making them.

         Assume each layer takes 2 minutes to prepare.
        """
        return 0

    def elapsed_time_in_minutes(self, number_of_layers:int, elapsed_bake_time:int):
        """
        Arguments:
            - number_of_layers (the number of layers added to the lasagna)
            - elapsed_bake_time (the number of minutes the lasagna has been baking in the oven).

        This function should return the total number of minutes you've been cooking,
        or the sum of your preparation time and the time the lasagna has already spent baking in the oven.
        """
        return 0