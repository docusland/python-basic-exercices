"""
Tests executed on the exercices.py file.
You should not code in this file unless you know what you are doing.
"""
import unittest
from exercices import *

class HtcToTTC_TestCase(unittest.TestCase):
    """ Unit tests for htc_to_ttc method. """

    def test_htc_to_ttc(self):
        """ Basic test. Given a value, the method calculates it's taxed value. """
        self.assertEqual(12.06, htc_to_ttc(10))

    def test_htc_to_ttc_with_discount(self):
        """ Given a value, and a discount rate, the method calculates it's taxed value. """
        self.assertEqual(10.85, htc_to_ttc(10, 0.1))

    def test_htc_to_ttc_with_invalid_discount(self):
        """ If incorrect parameters given. The function generates an Exception. """
        with self.assertRaises(Exception) as e_info:
            htc_to_ttc(10, 2)
        with self.assertRaises(Exception) as e_info:
            htc_to_ttc(10, -0.2)

if __name__ == '__main__':
    unittest.main()
