# Exercices de python

Vous disposez de plusieurs fichiers de tests. 
Ces fichiers contiennents des tests unitaires, il existe systématiquement un fichier ayant le même nom (sans le préfixe test_).
Votre objectif est de compléter les fonctions afin de pouvoir valider les tests. 

## HTC to TTC : 
Exercice de comptabilité. Convertir un devise hors taxe en toutes taxes

## Divisors : 
Exercice de mathématiques. Trouver les diviseurs communs d'un nombre.

## Lasagna : 
Exercice de découverte de la programmation orientée objet.

## Rational Numbers : 
Exercice de découverte des méthodes magiques au sein de la programmation orientée objet.

## Bonus : 
Exercices de pratiques permettant de pratiquer les boucles ou des méthodes natives du langage python.
